package sample.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import sample.Ingredient;
import sample.Taco;

@Repository
public class JdbcTacoRepository implements TacoRepository {
	
	private static final Logger log = LoggerFactory.getLogger(JdbcTacoRepository.class);
	
	private JdbcTemplate jdbc;
	
	@Autowired
	public JdbcTacoRepository(JdbcTemplate jdbc) {
		super();
		this.jdbc = jdbc;
	}

	@Override
	public Taco save(Taco taco) {
		long tacoId = saveTacoInfo(taco);
		taco.setId(tacoId);
		for(Ingredient ingredient : taco.getIngredients()) {
			saveIngredientToTaco(ingredient, tacoId);
		}
		return taco;
		
	}
	
	private long saveTacoInfo(Taco taco) {
		taco.setCreatedAt(new Date());
//		PreparedStatementCreator psc = 
//				new PreparedStatementCreatorFactory("insert into Taco (name, createdAt) values (?,?)", Types.VARCHAR, Types.TIMESTAMP)
//				.newPreparedStatementCreator(Arrays.asList(taco.getName(), new Timestamp(taco.getCreatedAt().getTime())));
		
		KeyHolder holder = new GeneratedKeyHolder();
		jdbc.update(new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
				PreparedStatement stat = con.prepareStatement("insert into Taco (name, createdAt) values (?,?)", new String[] {"id"});
				stat.setString(1, taco.getName());
				stat.setTimestamp(2, new Timestamp(taco.getCreatedAt().getTime()));
				return stat;
			}
		}, holder);
		
		log.info("key holder size: " + holder.getKeyList());
		
		return holder.getKey().longValue();
	}
	
	private void saveIngredientToTaco(Ingredient ingredient, long tacoId) {
		jdbc.update(
		        "insert into Taco_Ingredients (taco_id, ingredients_id) " +
		        "values (?, ?)",
		        tacoId, ingredient.getId());
	}

}
