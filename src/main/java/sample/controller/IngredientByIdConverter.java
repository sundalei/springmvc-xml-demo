package sample.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import sample.Ingredient;
import sample.data.IngredientRepository;

@Component
public class IngredientByIdConverter implements Converter<String, Ingredient> {
	
	private static final Logger log = LoggerFactory.getLogger(IngredientByIdConverter.class);

	@Autowired
	private IngredientRepository ingredientRepository;

	@Override
	public Ingredient convert(String id) {
		log.info("IngredientByIdConverter convert id: " + id);
		return this.ingredientRepository.findById(id);
	}

	public IngredientRepository getIngredientRepository() {
		return ingredientRepository;
	}

	public void setIngredientRepository(IngredientRepository ingredientRepository) {
		this.ingredientRepository = ingredientRepository;
	}
}
