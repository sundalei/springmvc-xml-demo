package sample.data;

import sample.Taco;

public interface TacoRepository {
	
	Taco save(Taco design);
}
