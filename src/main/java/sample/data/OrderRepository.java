package sample.data;

import sample.Order;

public interface OrderRepository {
	
	Order save(Order order);
}
