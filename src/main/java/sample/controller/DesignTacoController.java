package sample.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import sample.Ingredient;
import sample.Order;
import sample.Taco;
import sample.data.IngredientRepository;
import sample.data.TacoRepository;

@Controller
@RequestMapping("/design")
@SessionAttributes("order")
public class DesignTacoController {
	
	private static final Logger log = LoggerFactory.getLogger(DesignTacoController.class);
	
	private final IngredientRepository ingredientRepository;
	private TacoRepository tacoRepository;
	
	@Autowired
	public DesignTacoController(IngredientRepository ingredientRepository, TacoRepository tacoRepository) {

		this.ingredientRepository = ingredientRepository;
		this.tacoRepository = tacoRepository;
	}
	
	@ModelAttribute(name = "order")
	public Order order() {
		return new Order();
	}
	
	@ModelAttribute(name = "taco")
	public Taco taco() {
		return new Taco();
	}

	@GetMapping
	public String showDesignForm(Model model) {
		List<Ingredient> ingredients = new ArrayList<>();
		ingredientRepository.findAll().forEach(ingredient -> ingredients.add(ingredient));

		Ingredient.Type[] types = Ingredient.Type.values();
		for(Ingredient.Type type : types) {
			model.addAttribute(type.toString().toLowerCase(), filterByType(ingredients, type));
		}
		
		return "design";
	}
	
	@PostMapping
	public String processDesign(Taco design, @ModelAttribute Order order) {
		
		log.info("order: " + order);
		
		Taco saved = tacoRepository.save(design);
		order.addDesign(saved);
		
		//System.out.println("Processing design: " + design);
		log.info("Processing design: " + design);
		log.info("order taco: " + order.getTacos());
		
		return "redirect:/orders/current";
	}
	
	private List<Ingredient> filterByType(List<Ingredient> ingredients, Ingredient.Type type) {
		return ingredients.stream().filter(ingredient -> ingredient.getType().equals(type))
				.collect(Collectors.toList());
	}

}
